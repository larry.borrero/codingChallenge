package com.viseo.codingchallenge.controller;

import com.viseo.codingchallenge.model.TaxProfile;
import com.viseo.codingchallenge.service.TaxCalculator;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {
        TaxCalculator taxCalculator = new TaxCalculator();
        TaxProfile profileK1 = new TaxProfile("K1", new BigDecimal(63000000));
        TaxProfile profileTK0 = new TaxProfile("TK0", new BigDecimal(54000000));
        BigDecimal annualIncomeTax1 = taxCalculator.calculateAnnualTax(
            new BigDecimal(25000000),
            null
        );
        BigDecimal annualIncomeTax2 = taxCalculator.calculateAnnualTax(
            new BigDecimal(6500000),
            profileK1
        );
        BigDecimal annualIncomeTax3 = taxCalculator.calculateAnnualTax(
            new BigDecimal(25000000 ),
            profileTK0
        );

        System.out.println("annualIncomeTax1: " + annualIncomeTax1);
        System.out.println("annualIncomeTax2: " + annualIncomeTax2);
        System.out.println("annualIncomeTax3: " + annualIncomeTax3);
    }
}
