package com.viseo.codingchallenge.service;

import com.viseo.codingchallenge.model.Tax;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaxService {

    // Much better if Tax is saved in the database and fetch tax ranges based on annual income
    List<Tax> fetchTaxes(BigDecimal annualIncome) {
        List<Tax> taxes = new ArrayList<>();
        taxes.add(new Tax(
            new BigDecimal(0),
            new BigDecimal(50000000),
            0.05
        ));
        taxes.add(new Tax(
            new BigDecimal(50000000),
            new BigDecimal(250000000),
            0.15
        ));
        taxes.add(new Tax(
            new BigDecimal(250000000),
            new BigDecimal(500000000),
            0.25
        ));
        taxes.add(new Tax(
            new BigDecimal(500000000),
            null,
            0.30
        ));
        Collections.sort(taxes);
        List<Tax> filteredTaxes = new ArrayList<>();
        for (Tax tax : taxes) {
            if (annualIncome.compareTo(tax.getRangeFrom()) > 0) {
                filteredTaxes.add(tax);
            }
        }
        Collections.sort(filteredTaxes);
        return filteredTaxes;
    }
}
