package com.viseo.codingchallenge.service;

import com.viseo.codingchallenge.model.Tax;
import com.viseo.codingchallenge.model.TaxProfile;

import java.math.BigDecimal;
import java.util.List;

public class TaxCalculator {

    private static final Integer _ONE_YEAR_ = 12;
    TaxService taxService = new TaxService();

    public BigDecimal calculateAnnualTax(BigDecimal monthlyIncome, TaxProfile profile) {
        BigDecimal annualIncome = monthlyIncome.multiply(BigDecimal.valueOf(_ONE_YEAR_));
        BigDecimal taxableIncome = profile != null ? annualIncome.subtract(profile.getDeduction()) : annualIncome;
        List<Tax> taxes = taxService.fetchTaxes(taxableIncome);
        if (taxes.size() == 0 || monthlyIncome.equals(new BigDecimal(0))) {
            return new BigDecimal(0);
        }
        Tax maxTaxRange = getMaxRange(taxableIncome, taxes);
        BigDecimal totalIncomeTax = new BigDecimal(0);
        int counter = 0;
        Tax previousTax = null;
        for (Tax tax : taxes) {
            if (tax.getPercentage() <= maxTaxRange.getPercentage()) {
                if (counter == 0) {
                    if (taxableIncome.compareTo(tax.getRangeTo()) > 0) {
                        totalIncomeTax = totalIncomeTax.add(
                            tax.getRangeTo().multiply(
                                BigDecimal.valueOf(tax.getPercentage())
                            )
                        );
                        taxableIncome = taxableIncome.subtract(tax.getRangeTo());
                    } else {
                        totalIncomeTax = totalIncomeTax.add(
                            taxableIncome.multiply(
                                BigDecimal.valueOf(tax.getPercentage())
                            )
                        );
                    }
                } else {
                    if (taxableIncome.compareTo(tax.getRangeTo().subtract(previousTax.getRangeTo())) > 0) {
                        BigDecimal taxableAmount = tax.getRangeTo().subtract(previousTax.getRangeTo());
                        totalIncomeTax = totalIncomeTax.add(
                            taxableAmount.multiply(
                                BigDecimal.valueOf(tax.getPercentage())
                            )
                        );
                        taxableIncome = taxableIncome.subtract(taxableAmount);
                    } else {
                        totalIncomeTax = totalIncomeTax.add(
                            taxableIncome.multiply(
                                BigDecimal.valueOf(tax.getPercentage())
                            )
                        );
                    }
                }
                previousTax = taxes.get(counter);
                counter++;
            }
        }
        return totalIncomeTax;
    }

    private Tax getMaxRange(BigDecimal taxableIncome, List<Tax> taxes) {
        Tax maxTaxRange = null;
        for (Tax tax : taxes) {
            if (tax.getRangeTo() != null) {
                if (taxableIncome.compareTo(tax.getRangeFrom()) > 0 && taxableIncome.compareTo(tax.getRangeTo()) <= 0) {
                    maxTaxRange = tax;
                }
            } else {
                if (taxableIncome.compareTo(tax.getRangeFrom()) > 0) {
                    maxTaxRange = tax;
                }
            }
        }
        return maxTaxRange;
    }
}
