package com.viseo.codingchallenge.model;

import java.math.BigDecimal;

public class TaxProfile {

    private String name;
    private BigDecimal deduction;

    public TaxProfile(String name, BigDecimal deduction) {
        this.name = name;
        this.deduction = deduction;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDeduction(BigDecimal deduction) {
        this.deduction = deduction;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getDeduction() {
        return deduction;
    }
}
