package com.viseo.codingchallenge.model;

import java.math.BigDecimal;

public class Tax implements Comparable<Tax> {

    private BigDecimal rangeFrom;
    private BigDecimal rangeTo;
    private double percentage;

    public Tax(BigDecimal rangeFrom, BigDecimal rangeTo, double percentage) {
        this.rangeFrom = rangeFrom;
        this.rangeTo = rangeTo;
        this.percentage = percentage;
    }

    public void setRangeFrom(BigDecimal rangeFrom) {
        this.rangeFrom = rangeFrom;
    }

    public void setRangeTo(BigDecimal rangeTo) {
        this.rangeTo = rangeTo;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public BigDecimal getRangeFrom() {
        return rangeFrom;
    }

    public BigDecimal getRangeTo() {
        return rangeTo;
    }

    public double getPercentage() {
        return percentage;
    }

    @Override
    public int compareTo(Tax o) {
        if (getPercentage() == 0 || o.getPercentage() == 0) {
            return 0;
        }
        return getPercentage() < o.getPercentage() ? -1 : 1;
    }
}
